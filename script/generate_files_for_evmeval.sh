#!/usr/bin/env bash
set -e

cd "$(dirname ${0})/.."

config="script/coreference.conf"

sbt -mem 20480 "run-main takkbp.TbfFormatter -c \"${config}\""
sbt -mem 20480 "run-main takkbp.TknGenerator -c \"${config}\""
