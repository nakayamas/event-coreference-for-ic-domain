#!/usr/bin/env bash
set -e

cd "$(dirname ${0})/.."

config="script/coreference.conf"
output="result.perceptron.$(date +'%Y-%m-%d-%H%M')"

sbt -mem 20480 "run-main coreference.PerceptronCoreferenceResolver -c \"${config}\" -o \"${output}\""
