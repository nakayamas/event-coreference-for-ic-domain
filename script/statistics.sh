#!/usr/bin/env bash
set -e

cd "$(dirname ${0})/.."

config="script/coreference.conf"

sbt -mem 20480 "run-main statistics.Statistics -c \"${config}\""
