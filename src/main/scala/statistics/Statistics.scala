package statistics

import java.io.File

import config.EventCoreferenceConfig
import doc.{EpistemicStatus, Event, EventCoreference, Doc}
import reader.SourceFilesReader
import scopt.OptionParser
import util.FileOps

import scala.util.Random

/**
  * Created by nakayama.
  */
object Statistics {

    def main(args: Array[String]): Unit = {
        val options: ArgOption = argParser(args)

        // Parse args
        if (options.config != null) {
            EventCoreferenceConfig.set(options.config)
        }

        val icDomainFiles: Array[File] = EventCoreferenceConfig.icDomainDir match {
            case Some(d) => FileOps.getFilesFromDir(d)
            case _ => throw new RuntimeException("IC domain dir setting was not found.")
        }
        val sourceFiles: Array[File] = EventCoreferenceConfig.sourceFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Source file extension of the IC domain setting was not found.")
        }
        val annotationFiles: Array[File] = EventCoreferenceConfig.annotationFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Annotation file extension of the IC domain setting was not found.")
        }

        // Prepare training and prediction data
        Random.setSeed(1)
        val shuffledSourceFiles: Array[File] = Random.shuffle(sourceFiles.toSeq).toArray
        val sourceFilesForTraining: Array[File] =
            shuffledSourceFiles.splitAt(EventCoreferenceConfig.numberOfTrainingFiles)._1.sorted

        val docs: Array[Doc] = new SourceFilesReader(sourceFilesForTraining).parseWithAnnotationFiles(annotationFiles)

        val result = statistics(docs)

        FileOps.write(System.out)(_.println(result))
    }

    final def statistics(docs: Seq[Doc]): String = {
        val output: StringBuilder = new StringBuilder
        // Count number of events
        val allEvents: Seq[Event] = docs.flatMap(_.events)
        output.append("number of events: " + allEvents.size + "\n")

        // Count number of coreference clusters
        val allEventCoreferences: Seq[EventCoreference] = docs.flatMap(_.eventCoreferences)
        output.append("number of coreference clusters: " + allEventCoreferences.size + "\n")
        output.append("number of coreference clusters which have greater than or equal to two events: "
            + allEventCoreferences.count(_.events.size >= 2) + "\n")

        // Count number of coreference clusters which have different epistemic status in the events
        val irregularCoreferences: Seq[EventCoreference] =
            allEventCoreferences.filter(_.events.map(_.epistemicStatus).toSet.size != 1)
        output.append("number of coreference clusters which have different epistemic status in the events: ")
        output.append(irregularCoreferences.size + "\n")
        irregularCoreferences.foreach { c =>
            output.append("\n")
            output.append(c.events.head.tokens.head.doc.id + "\n")
            output.append(c.events.map(e => (e.text, e.epistemicStatus.get, e.tokens.head.startOffset)) + "\n")
        }

        // Epistemic status rate
        val ao: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.AO))
        val no: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.NO))
        val edo: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.EDO))
        val nedo: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.NEDO))
        val oo: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.OO))
        val noo: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.NOO))
        val co: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.CO))
        val nco: Seq[Event] = allEvents.filter(_.epistemicStatus.contains(EpistemicStatus.NCO))
        output.append("AO: " + ao.size + " " + (ao.size.toDouble / allEvents.size) + "\n")
        output.append("NO: " + no.size + " " + (no.size.toDouble / allEvents.size) + "\n")
        output.append("EDO: " + edo.size + " " + (edo.size.toDouble / allEvents.size) + "\n")
        output.append("NEDO: " + nedo.size + " " + (nedo.size.toDouble / allEvents.size) + "\n")
        output.append("OO: " + oo.size + " " + (oo.size.toDouble / allEvents.size) + "\n")
        output.append("NOO: " + noo.size + " " + (noo.size.toDouble / allEvents.size) + "\n")
        output.append("CO: " + co.size + " " + (co.size.toDouble / allEvents.size) + "\n")
        output.append("NCO: " + nco.size + " " + (nco.size.toDouble / allEvents.size) + "\n")

//        docs.find(_.id == "XIN_ENG_19980405.0022") match {
//            case Some(d) => d.eventCoreferences.foreach { c =>
//                println(c.events.map(e => (e.text, e.epistemicStatus.get, e.tokens.head.startOffset)) + "\n")
//            }
//            case _ => println("not found")
//        }

        output.result
    }

    final private[this] case class ArgOption(config: File = null)

    final private[this] def argParser(args: Array[String]) = {
        new OptionParser[ArgOption](this.getClass.getName) {
            opt[File]('c', "config")
                .action((x, c) => c.copy(config = x))
                .validate { x =>
                    if (!x.exists) {
                        failure("configuration file was not found: " + x.getAbsolutePath)
                    } else success
                }
                .text("configuration file")
        }.parse(args, ArgOption()) match {
            case Some(x) => x
            case _ => System.exit(-1); null
        }
    }

}
