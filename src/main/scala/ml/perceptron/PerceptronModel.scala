package ml.perceptron

import coreference.CoreferenceFeatureExtractorForPerceptron
import doc.{CoreferenceTree, Doc}

import scala.collection.mutable

/**
  * Created by nakayama.
  */
case class PerceptronModel(weights: Map[String, Double], parameter: Parameter)

object PerceptronModel {

    /**
      * Train with given docs and parameter
      *
      * @param docs      documents
      * @param parameter training parameter
      * @return a model which is trained by documents with the parameter
      */
    def train(docs: Array[Doc], parameter: Parameter) = {
        val featureExtractor = CoreferenceFeatureExtractorForPerceptron(docs)

        val predictor: Predictor = new Predictor

        val weights = mutable.Map.empty[String, Double]
        val averageWeights = mutable.Map.empty[String, Double]

        for {
            n <- Range(0, parameter.numberOfIterations)
            doc <- docs
            goldTree: CoreferenceTree <- featureExtractor.docToCoreferenceTree.get(doc)
            goldFeatures = goldTree.features
        } {
            println(s"${n} iteration...")
            val predictedTree: CoreferenceTree = predictor.predict(doc, weights.toMap, featureExtractor, parameter.beamWidth)
            if (goldTree != predictedTree) {
                val incorrectFeatures: Map[String, Double] = predictedTree.features
                goldFeatures.keys.foreach { k =>
                    weights(k) = weights.getOrElseUpdate(k, 0) + goldFeatures(k) - incorrectFeatures(k)
                }
            }
            weights.keys.foreach { k =>
                averageWeights(k) = averageWeights.getOrElseUpdate(k, 0) + weights(k)
            }
        }

        if (parameter.solverType == SolverType.AVERAGE) {
            val times: Int = parameter.numberOfIterations * docs.length
            averageWeights.keys.foreach { k =>
                weights(k) = averageWeights(k) / times
            }
        }

        new PerceptronModel(weights.toMap, parameter)
    }

    /**
      * Given a set of weights and features, return the score
      * w * f(e), where e is an event
      *
      * @param weights  weights for perceptron
      * @param features features for perceptron
      * @return a score of perceptron
      */
    def score(weights: Map[String, Double], features: Map[String, Double]): Double =
        weights.keys.foldLeft(0.0)((z, k) => z + (weights(k) * features.getOrElse(k, 0.0)))

}
