package ml.perceptron

/**
  * Created by nakayama.
  */
case class Parameter(solverType: SolverType.Value, numberOfIterations: Int, beamWidth: Int)
