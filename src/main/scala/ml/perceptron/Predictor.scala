package ml.perceptron

import coreference.CoreferenceFeatureExtractorForPerceptron
import doc.{CoreferenceTree, Doc, Event, EventCoreference}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
case class Predictor() {

    def predict(doc: Doc, weights: Map[String, Double], featureExtractor: CoreferenceFeatureExtractorForPerceptron, beamWidth: Int): CoreferenceTree =
        bestFirst(doc, weights, featureExtractor, beamWidth)

    type EventCoreferenceCluster = Array[Event]
    type EventCoreferenceTree = Array[EventCoreferenceCluster]

    def bestFirst(doc: Doc, weights: Map[String, Double], featureExtractor: CoreferenceFeatureExtractorForPerceptron, beamWidth: Int): CoreferenceTree = {
        def createCoreferenceTreeSet(coreferenceTreeSet: Array[EventCoreferenceTree], event: Event): Array[EventCoreferenceTree] = {
            val newCoreferenceTreeSet: ArrayBuffer[EventCoreferenceTree] = ArrayBuffer[EventCoreferenceTree]()
            for (coreferenceTree <- coreferenceTreeSet) {
                newCoreferenceTreeSet += coreferenceTree :+ Array(event)
                for {
                    i <- coreferenceTree.indices
                    (heads, tails) = coreferenceTree.splitAt(i)
                } {
                    newCoreferenceTreeSet +=
                        heads ++ Array(coreferenceTree(i) :+ event) ++ tails.tail
                }
            }

//            println("===== generated trees ======")
//            newCoreferenceTreeSet.foreach { tree =>
//                println("tree")
//                tree.foreach(c => println("->" + c.map(_.eventId).mkString("\t")))
//                println
//            }
//            println("============================")

            newCoreferenceTreeSet.toArray
        }

        val depth: Iterator[Int] = Stream.from(0).iterator

        var coreferenceTreeSet = Array[(EventCoreferenceTree, Map[String, Double], Double)]((Array(), Map(), 0.0))
        doc.events.foreach { e =>
            // Create possible coreference trees
            coreferenceTreeSet = createCoreferenceTreeSet(coreferenceTreeSet.map(_._1), e)
                // Evaluate
                .map { tree =>
                    val features: Map[String, Double] = featureExtractor.extractFeatures(tree)
                    (tree, features, PerceptronModel.score(weights, features))
                }
                // Take best trees
                .sortBy(_._3).take(beamWidth)
//            println("search depth: " + depth.next + ", set size: " + coreferenceTreeSet.length)
        }
        val eventCoreferenceClusters: (Array[Array[Event]], Map[String, Double], Double) = coreferenceTreeSet.maxBy(_._3)
        new CoreferenceTree(doc, eventCoreferenceClusters._1.map(new EventCoreference(_)), eventCoreferenceClusters._2)
    }

}
