package doc

/**
  * Created by nakayama.
  */
final case class Sentence(id: Int,
                          text: String,
                          tokens: Seq[Token]) {

    private var _doc: Doc = null

    def doc: Doc = _doc

    def doc_=(doc: Doc) = _doc = doc

    tokens.foreach(_.sentence = this)

}
