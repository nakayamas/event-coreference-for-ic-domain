package doc

/**
  * Created by nakayama.
  */
final case class EventElement(eventId: Int,
                       surface: String,
                       private val originalEventType: Option[EventType.Value],
                       private val originalEpistemicStatus: Option[EpistemicStatus.Value],
                       private val originalParentEventId: Option[Int],
                       private val originalInReportingId: Seq[Int],
                       private val originalCoreferenceId: Option[Int],
                       private val originalMemberIds: Seq[Int],
                       private val originalElliptical: Boolean
                      ) {

    private var _token: Token = null

    def token: Token = _token

    def token_=(token: Token) = _token = token

    private var _event: Event = null

    def event: Event = _event

    def event_=(event: Event) = _event = event

    lazy val sentence: Sentence = token.sentence

    lazy val doc: Doc = sentence.doc

    // Get tokens which have the same event id
    lazy val eventTokens: Seq[Token] =
        doc.eventTokens.filter(_.eventElement.get.eventId == eventId).sortBy(_.startOffset)

    lazy val events: Seq[EventElement] = eventTokens.map(_.eventElement.get)

    lazy val eventType: Option[EventType.Value] =
        events.find(_.originalEventType.isDefined) match {
            case Some(e) => e.originalEventType
            case _ => None
        }

    lazy val epistemicStatus: Option[EpistemicStatus.Value] =
        events.find(_.originalEpistemicStatus.isDefined) match {
            case Some(e) => e.originalEpistemicStatus
            case _ => None
        }

    lazy val parentEventId: Option[Int] =
        events.find(_.originalParentEventId.isDefined) match {
            case Some(e) => e.originalParentEventId
            case _ => None
        }

    lazy val inReportingId: Seq[Int] = events.flatMap(_.originalInReportingId)

    lazy val coreferenceId: Option[Int] =
        events.find(_.originalCoreferenceId.isDefined) match {
            case Some(e) => e.originalCoreferenceId
            case _ => None
        }

    lazy val memberIds: Seq[Int] = events.flatMap(_.originalMemberIds)

    lazy val isElliptical: Boolean = events.exists(_.originalElliptical)

}
