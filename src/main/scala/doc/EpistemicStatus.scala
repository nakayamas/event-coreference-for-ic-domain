package doc

/**
  * Created by nakayama.
  */
object EpistemicStatus extends Enumeration {

    final val AO, NO, EDO, NEDO, OO, NOO, CO, NCO = Value

    final private[this] val abbreviations: Map[String, EpistemicStatus.Value] =
        Map("actual_occurrence" -> AO,
            "negated_occurrence" -> NO,
            "expected_occurrence" -> EDO,
            "negated_expected" -> NEDO) ++ values.map(a => (a.toString, a)).toMap

    final def uniformWithName(s: String): Option[EpistemicStatus.Value] = abbreviations.get(s)

}
