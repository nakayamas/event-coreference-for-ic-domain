package doc

/**
  * Created by nakayama.
  */
final case class Token(index: Int,
                       startOffset: Int,
                       endOffset: Int,
                       surface: String,
                       lemma: String,
                       pos: String,
                       pos2: String,
                       ne: Option[String]) {

    private var _doc: Doc = null

    def doc: Doc = _doc

    def doc_=(doc: Doc) = _doc = doc

    private var _sentence: Sentence = null

    def sentence: Sentence = _sentence

    def sentence_=(sentence: Sentence) = _sentence = sentence

    private var _eventElement: Option[EventElement] = None

    def eventElement: Option[EventElement] = _eventElement

    def eventElement_=(eventElement: Option[EventElement]) = {
        eventElement match {
            case Some(e) => e.token = this
            case _ =>
        }
        _eventElement = eventElement
    }

    lazy val indexWithInSentence: Int = sentence.tokens.indexWhere(_ == this)

    lazy val prevToken: Option[Token] = {
        if (indexWithInSentence == 0) {
            None
        } else {
            Some(sentence.tokens(indexWithInSentence - 1))
        }
    }

    lazy val nextToken: Option[Token] = {
        val tokens: Seq[Token] = sentence.tokens
        if (tokens.length - 1 == indexWithInSentence) {
            None
        } else {
            Some(tokens(indexWithInSentence + 1))
        }
    }

}
