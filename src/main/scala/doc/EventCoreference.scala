package doc

/**
  * Created by nakayama.
  */
case class EventCoreference(events: Seq[Event]) {

    def contains(otherEvent: Event): Boolean = events.contains(otherEvent)

}
