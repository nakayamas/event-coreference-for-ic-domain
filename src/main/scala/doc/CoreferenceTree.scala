package doc

/**
  * Created by nakayama.
  */
final case class CoreferenceTree(doc: Doc, eventCoreferences: Array[EventCoreference], features: Map[String, Double]) {

    override def hashCode: Int = eventCoreferences
        // Summation of events of each cluster
        .map(_.events.foldLeft(1)((z, n) => z * (n.hashCode + 31)))
        // Summation of clusters
        .foldLeft(1)((z, n) => z * (n + 31))

    private val hashCodeValue: Int = hashCode()

    override def equals(other: Any): Boolean = other match {
        case that: CoreferenceTree =>
            (that canEqual this) && (this.hashCodeValue == that.hashCodeValue)
        case _ => false
    }

    override def canEqual(other: Any): Boolean = other.isInstanceOf[CoreferenceTree]

    var score: Double = 0.0

}
