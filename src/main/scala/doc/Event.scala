package doc

/**
  * Created by nakayama.
  */
case class Event(eventElements: Seq[EventElement]) {

    eventElements.foreach(_.event = this)

    val eventId: Int = eventElements.headOption match {
        case Some(e) => e.eventId
        case _ => 0
    }

    val epistemicStatus: Option[EpistemicStatus.Value] = eventElements.headOption match {
        case Some(e) => e.epistemicStatus
        case _ => None
    }

    val eventType: Option[EventType.Value] = eventElements.headOption match {
        case Some(e) => e.eventType
        case _ => None
    }

    val parentEventId: Option[Int] = eventElements.headOption match {
        case Some(e) => e.parentEventId
        case _ => None
    }

    val inReportingId: Seq[Int] = eventElements.headOption match {
        case Some(e) => e.inReportingId
        case _ => Nil
    }

    val coreferenceId: Option[Int] = eventElements.headOption match {
        case Some(e) => e.coreferenceId
        case _ => None
    }

    val memberIds: Seq[Int] = eventElements.headOption match {
        case Some(e) => e.memberIds
        case _ => Nil
    }

    val isElliptical: Boolean = eventElements.headOption match {
        case Some(e) => e.isElliptical
        case _ => false
    }

    val tokens: Seq[Token] = eventElements.map(_.token)

    // TODO: calculate filling space length
    val text: String = tokens.map(_.surface).mkString(" ")

    private var _eventCoreference: EventCoreference = null

    def eventCoreference: EventCoreference = _eventCoreference

    def eventCoreference_=(eventCoreference: EventCoreference) = _eventCoreference = eventCoreference

    def coreferTo(otherEvent: Event): Boolean = eventCoreference.contains(otherEvent)

    val headToken: Token = tokens.headOption match {
        case Some(t) => t
        case _ => throw new RuntimeException(s"Missing tokens in event: ${eventElements}")
    }

    val docOfHeadToken: Doc = headToken.doc

    val sentenceOfHeadToken: Sentence = headToken.sentence

    val headPos: String = headToken.pos

}
