package doc

import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
final case class Doc(id: String,
                     docType: Symbol,
                     date: String,
                     sentences: Seq[Sentence],
                     rawText: String) {

    sentences.foreach(_.doc = this)

    val tokens: Seq[Token] = sentences.flatMap(_.tokens)

    tokens.foreach(_.doc = this)

    val text: String = sentences.map(_.text).mkString(" ")

    val eventTokens: Seq[Token] = sentences.flatMap(_.tokens).filter(_.eventElement.isDefined)

    val events: Array[Event] = {
        val eventElements: Seq[EventElement] = tokens.collect {
            case t if t.eventElement.isDefined => t.eventElement.get
        }
        // Collect events which have same eventId
        eventElements.groupBy(_.eventId)
            // Construct a Event instance (sort event elements by the token index)
            .map(evntElms => new Event(evntElms._2.sortBy(_.token.index)))
            // Convert the type from Iterable to List
            .toIndexedSeq
            // Sort Events by appearance order in the text
            .sortBy(_.eventElements.head.token.index)
            .toArray
    }

    val eventCoreferences: Array[EventCoreference] = {
        val eventCoreferences: ArrayBuffer[ArrayBuffer[Event]] = ArrayBuffer[ArrayBuffer[Event]]()
        for (event <- events) {
            event.coreferenceId match {
                case Some(corefId) => eventCoreferences.find(_.exists(_.eventId == corefId)) match {
                    case Some(eventCoref) => eventCoref += event
                    case _ => eventCoreferences += ArrayBuffer(event)
                }
                case _ => eventCoreferences += ArrayBuffer(event)
            }
        }
        eventCoreferences.map(new EventCoreference(_)).toArray
    }

    for {
        eventCoreference <- eventCoreferences
        event <- eventCoreference.events
    } event.eventCoreference = eventCoreference

}
