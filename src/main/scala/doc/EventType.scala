package doc

/**
  * Created by nakayama.
  */
object EventType extends Enumeration {

    val EVENT, IMPLICIT, REPORT = Value

    def uniformWithName(s: String): Option[EventType.Value] = values.find(_.toString == s.toUpperCase)

}
