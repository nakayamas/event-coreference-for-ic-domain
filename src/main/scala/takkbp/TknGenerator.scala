package takkbp

import java.io.File

import config.EventCoreferenceConfig
import doc.Doc
import reader.SourceFilesReader
import scopt.OptionParser
import util.FileOps

/**
  * Created by nakayama.
  */
object TknGenerator {

    def main(args: Array[String]): Unit = {
        val options: ArgOption = argParser(args)

        // Parse args
        if (options.config != null) {
            EventCoreferenceConfig.set(options.config)
        }

        val icDomainFiles: Array[File] = EventCoreferenceConfig.icDomainDir match {
            case Some(d) => FileOps.getFilesFromDir(d)
            case _ => throw new RuntimeException("IC domain dir setting was not found.")
        }
        val sourceFiles: Array[File] = EventCoreferenceConfig.sourceFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Source file extension of the IC domain setting was not found.")
        }
        val annotationFiles: Array[File] = EventCoreferenceConfig.annotationFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Annotation file extension of the IC domain setting was not found.")
        }

        val docs: Array[Doc] = new SourceFilesReader(sourceFiles).parseWithAnnotationFiles(annotationFiles)

        val tknDir: File = EventCoreferenceConfig.tknDirPath match {
            case Some(p) => new File(p)
            case _ => throw new RuntimeException("tkn dir path setting was not found.")
        }
        tknDir.mkdirs()

        docs.foreach { d =>
            val outputFile: File = new File(s"${tknDir.getAbsolutePath}/${d.id}.tab")
            FileOps.write(outputFile)(_.println(docToTkn(d)))
        }
    }

    final def docToTkn(doc: Doc): String = doc.tokens.map { t =>
        Seq(t.index + 1, t.surface, t.startOffset, t.endOffset).mkString("\t")
    }.mkString("\n")

    final private[this] case class ArgOption(config: File = null)

    final private[this] def argParser(args: Array[String]) = {
        new OptionParser[ArgOption](this.getClass.getName) {
            opt[File]('c', "config")
                .action((x, c) => c.copy(config = x))
                .validate { x =>
                    if (!x.exists) {
                        failure("configuration file was not found: " + x.getAbsolutePath)
                    } else success
                }
                .text("configuration file")
        }.parse(args, ArgOption()) match {
            case Some(x) => x
            case _ => System.exit(-1); null
        }
    }

}
