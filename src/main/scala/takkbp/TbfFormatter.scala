package takkbp

import java.io.File

import config.EventCoreferenceConfig
import doc.{Doc, Event}
import reader.SourceFilesReader
import scopt.OptionParser
import util.FileOps

/**
  * Created by nakayama.
  */
object TbfFormatter {

    def main(args: Array[String]): Unit = {
        val options: ArgOption = argParser(args)

        // Parse args
        if (options.config != null) {
            EventCoreferenceConfig.set(options.config)
        }

        val icDomainFiles: Array[File] = EventCoreferenceConfig.icDomainDir match {
            case Some(d) => FileOps.getFilesFromDir(d)
            case _ => throw new RuntimeException("IC domain dir setting was not found.")
        }
        val sourceFiles: Array[File] = EventCoreferenceConfig.sourceFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Source file extension of the IC domain setting was not found.")
        }
        val annotationFiles: Array[File] = EventCoreferenceConfig.annotationFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Annotation file extension of the IC domain setting was not found.")
        }

        val docs: Array[Doc] = new SourceFilesReader(sourceFiles).parseWithAnnotationFiles(annotationFiles)

        val goldTbf: File = EventCoreferenceConfig.goldTbfPath match {
            case Some(p) => new File(p)
            case _ => throw new RuntimeException("gold.tbf path setting was not found.")
        }

        FileOps.writeWithMakeParentDirs(goldTbf)(_.print(tbfFormatter(docs)))
    }

    final def tbfFormatter(docs: Seq[Doc]): String = {
        val tbf = new StringBuilder()
        for (doc <- docs) {
            tbf.append("#BeginOfDocument " + doc.id + "\n")
            for ((eventCoreference, eventCoreferenceId) <- doc.eventCoreferences.zipWithIndex) {
                for (event <- eventCoreference.events) {
                    val epistemicStatus: String = event.epistemicStatus match {
                        case Some(s) => s.toString
                        case _ => throw new RuntimeException(s"Missing epistemic status at ${event.text} in ${doc.id}")
                    }
                    tbf.append(Seq("system1", doc.id,
                        "E" + event.eventId, event.tokens.map(_.index + 1).mkString(","), event.text,
                        "event_type", epistemicStatus).mkString("\t"))
                    tbf.append("\n")
                }
                // coreferences
                tbf.append(Seq("@Coreference", "C" + (eventCoreferenceId + 1),
                    eventCoreference.events.map("E" + _.eventId).mkString(",")).mkString("\t"))
                tbf.append("\n")
            }
            tbf.append("#EndOfDocument\n")
        }
        tbf.result()
    }

    def tbfFormatter(doc: Doc, eventCoreferences: Array[Array[Event]]): String = {
        val tbf = new StringBuilder()
        tbf.append("#BeginOfDocument " + doc.id + "\n")
        for ((eventCoreference, eventCoreferenceId) <- eventCoreferences.zipWithIndex) {
            for (event <- eventCoreference) {
                val epistemicStatus: String = event.epistemicStatus match {
                    case Some(s) => s.toString
                    case _ => throw new RuntimeException(s"Missing epistemic status at ${event.text} in ${doc.id}")
                }
                tbf.append(Seq("system1", doc.id,
                    "E" + event.eventId, event.tokens.map(_.index + 1).mkString(","), event.text,
                    "event_type", epistemicStatus).mkString("\t"))
                tbf.append("\n")
            }
            // coreferences
            tbf.append(Seq("@Coreference", "C" + (eventCoreferenceId + 1),
                eventCoreference.map("E" + _.eventId).mkString(",")).mkString("\t"))
            tbf.append("\n")
        }
        tbf.append("#EndOfDocument\n")
        tbf.result()
    }


    final private[this] case class ArgOption(config: File = null)

    final private[this] def argParser(args: Array[String]) = {
        new OptionParser[ArgOption](this.getClass.getName) {
            opt[File]('c', "config")
                .action((x, c) => c.copy(config = x))
                .validate { x =>
                    if (!x.exists) {
                        failure("configuration file was not found: " + x.getAbsolutePath)
                    } else success
                }
                .text("configuration file")
        }.parse(args, ArgOption()) match {
            case Some(x) => x
            case _ => System.exit(-1); null
        }
    }

}
