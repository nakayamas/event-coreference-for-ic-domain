package config

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._

/**
  * Created by nakayama.
  */
object EventCoreferenceConfig {

    final private[this] var config: Config = ConfigFactory.load()

    final def set(configFile: File): Unit =
        config = ConfigFactory.load(ConfigFactory.parseFile(configFile))

    // IC domain corpus config
    final private[this] lazy val icDomainConfig: Option[Config] = config.as[Option[Config]]("ic-domain")

    final lazy val icDomainDir: Option[String] =
        icDomainConfig match {
            case Some(c) => c.as[Option[String]]("base-dir")
            case _ => None
        }
    final lazy val annotationFileExtension: Option[String] =
        icDomainConfig match {
            case Some(c) => c.as[Option[String]]("ann-ext")
            case _ => None
        }
    final lazy val sourceFileExtension: Option[String] =
        icDomainConfig match {
            case Some(c) => c.as[Option[String]]("src-ext")
            case _ => None
        }

    final lazy val numberOfTrainingFiles: Int =
        icDomainConfig match {
            case Some(c) => c.as[Int]("number-of-training-files")
            case _ => 0
        }
    final lazy val numberOfPredictionFiles: Int =
        icDomainConfig match {
            case Some(c) => c.as[Int]("number-of-prediction-files")
            case _ => 0
        }


    // Coreference resolver config
    final private[this] lazy val coreferenceSvmConfig: Option[Config] = config.as[Option[Config]]("coreference-svm")

    final lazy val coreferenceSvmTrainCost: Option[Double] =
        coreferenceSvmConfig match {
            case Some(c) => c.as[Option[Double]]("train-cost")
            case _ => None
        }
    final lazy val coreferenceSvmTrainBias: Option[Double] =
        coreferenceSvmConfig match {
            case Some(c) => c.as[Option[Double]]("train-bias")
            case _ => None
        }
    final lazy val coreferenceSvmModelFile: Option[String] =
        coreferenceSvmConfig match {
            case Some(c) => c.as[Option[String]]("model-file")
            case _ => None
        }
    final lazy val coreferenceSvmLiblinearFeatureFile: Option[String] =
        coreferenceSvmConfig match {
            case Some(c) => c.as[Option[String]]("feature-file")
            case _ => None
        }
    final lazy val coreferenceSvmClassLabelIndexFile: Option[String] =
        coreferenceSvmConfig match {
            case Some(c) => c.as[Option[String]]("class-label-index-file")
            case _ => None
        }
    final lazy val coreferenceSvmFeatureNameIndexFile: Option[String] =
        coreferenceSvmConfig match {
            case Some(c) => c.as[Option[String]]("feature-name-index-file")
            case _ => None
        }

    final private[this] lazy val coreferencePerceptronConfig: Option[Config] = config.as[Option[Config]]("coreference-perceptron")

    final lazy val coreferencePerceptronSolverType: Option[String] =
        coreferencePerceptronConfig match {
            case Some(c) => c.as[Option[String]]("solver-type")
            case _ => None
        }
    final lazy val coreferencePerceptronNumberOfIterations: Option[Int] =
        coreferencePerceptronConfig match {
            case Some(c) => c.as[Option[Int]]("number-of-iterations")
            case _ => None
        }
    final lazy val coreferencePerceptronBeamWidth: Option[Int] =
        coreferencePerceptronConfig match {
            case Some(c) => c.as[Option[Int]]("beam-width")
            case _ => None
        }


    // TBF settings for TAK KBP
    final private[this] lazy val tacKbpConfig: Option[Config] = config.as[Option[Config]]("tac-kbp")

    final lazy val goldTbfPath: Option[String] =
        tacKbpConfig match {
            case Some(c) => c.as[Option[String]]("gold-tbf")
            case _ => None
        }
    final lazy val tknDirPath: Option[String] =
        tacKbpConfig match {
            case Some(c) => c.as[Option[String]]("tkn-dir")
            case _ => None
        }


    // Evaluation settings
    final private[this] lazy val evaluationConfig: Option[Config] = config.as[Option[Config]]("evaluation")

    final lazy val evaluationScript: Option[String] =
        evaluationConfig match {
            case Some(c) => c.as[Option[String]]("script")
            case _ => None
        }
    final lazy val visualizerScript: Option[String] =
        evaluationConfig match {
            case Some(c) => c.as[Option[String]]("visualizer")
            case _ => None
        }
    final lazy val visualizationTemplate: Option[String] =
        evaluationConfig match {
            case Some(c) => c.as[Option[String]]("visualization-template")
            case _ => None
        }


    // Output
    final lazy val outputDir: Option[String] = config.as[Option[String]]("output-dir")


    // Intermediate config
    final private[this] lazy val intermediateConfig: Option[Config] = config.as[Option[Config]]("intermediate")

    final lazy val useIntermediate: Boolean =
        intermediateConfig match {
            case Some(c) => c.as[Boolean]("use")
            case _ => false
        }
    final lazy val saveIntermediate: Boolean =
        intermediateConfig match {
            case Some(c) => c.as[Boolean]("save")
            case _ => false
        }
    final lazy val intermediateDir: Option[String] =
        intermediateConfig match {
            case Some(c) => c.as[Option[String]]("dir")
            case _ => None
        }

}
