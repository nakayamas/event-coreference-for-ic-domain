package reader

import java.io.{File, FileNotFoundException}

import config.EventCoreferenceConfig
import doc.Doc
import util.FileOps
import util.nlp.CoreNLP

/**
  * Created by nakayama.
  */
final class SourceFilesReader(sourceFiles: Array[File]) {

    def parseWithAnnotationFiles(annotationFiles: Array[File]): Array[Doc] = {
        // Load CoreNLP XML
        val coreNLPXMLDirPath = EventCoreferenceConfig.intermediateDir match {
            case Some(p) => p
            case _ => "."
        }
        if (EventCoreferenceConfig.useIntermediate && new File(coreNLPXMLDirPath).exists) {
            FileOps.getFilesFromDir(coreNLPXMLDirPath).foreach(CoreNLP.loadXML)
        }

        val docs: Array[Doc] = sourceFiles.map { f =>
            val sourceFileName: String = f.getName

            // Parse annotation file
            val annotationFile: File = annotationFiles.find(_.getName.contains(sourceFileName)) match {
                case Some(x) => x
                case _ => throw new FileNotFoundException("Annotation file of " + sourceFileName + " was not found.")
            }

            // Parse source file with annotation file
            SourceFileReader.parseWithAnnotationFile(f, annotationFile)
        }

        // Save CoreNLP XML
        if (EventCoreferenceConfig.saveIntermediate) {
            CoreNLP.writeAsXML(coreNLPXMLDirPath)
        }

        docs
    }

}
