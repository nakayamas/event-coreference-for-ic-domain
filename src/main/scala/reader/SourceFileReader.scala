package reader

import java.io.File

import doc._
import util.nlp.{CoreNLP, CoreNLPSentence}

import scala.collection.mutable
import scala.io.Source
import scala.util.Either.RightProjection
import scala.xml.{NodeSeq, XML}

/**
  * Created by nakayama.
  */
object SourceFileReader {

    case class SourceFile(docId: String,
                                  docType: Symbol,
                                  date: String,
                                  normalizedText: String)

    def parse(sourceFile: File): SourceFile = {
        // Parse the source xml
        val root: xml.Elem = XML.loadFile(sourceFile)
        val docId: String = (root \\ "@id").text
        val docType: Symbol = Symbol((root \\ "@type").text)
        val date: String = (root \\ "DATETIME").text.trim
        val headline: String = (root \\ "HEADLINE").text.trim
        val paragraphs: Seq[String] = (root \\ "TEXT" \\ "P").map(_.text.trim)

        // Add a period to the tail of a headline sentence to parse correctly with Stanford CoreNLP
        val text: String = headline.replaceAll("\n", " ") + ". " + paragraphs.mkString("\n").replaceAll("\n", " ")

        var normalizedText: String = text
        normalizedText = normalizedText.replaceAllLiterally("''", "\"")
        normalizedText = normalizedText.replaceAllLiterally("``", "\"")
        normalizedText = normalizedText.replaceAllLiterally("`", "'")
        normalizedText = normalizedText.replaceAll(" +", " ")

        new SourceFile(docId, docType, date, normalizedText)
    }

    // parseWithCoreNLP?
    def parseWithAnnotationFile(sourceFile: File, annotationFile: File): Doc = {
        // Read a source file
        val rawText: String = Source.fromFile(sourceFile).getLines().mkString("\n")
        val sourceFileName: String = sourceFile.getName
        val source = parse(sourceFile)

        val sentencesWithCoreNLP: Seq[CoreNLPSentence] = CoreNLP(source.normalizedText)

        def getOrThrowError[T](option: Option[T], message: String): RightProjection[Nothing, T] =
            option.toRight(throw new RuntimeException(message)).right
        val tokenIndex: Iterator[Int] = Stream.from(0).iterator
        val sentences: Seq[Sentence] = sentencesWithCoreNLP.zipWithIndex.map { case (s, i) =>
            val tokens: Seq[Token] = s.tokens.map { t =>
                (for {
                    startOffset <- getOrThrowError(t.startOffset, "startOffset was not set in: " + sourceFileName)
                    endOffset <- getOrThrowError(t.endOffset, "endOffset was not set in: " + sourceFileName)
                    surface <- getOrThrowError(t.surface, "surface was not set in: " + sourceFileName)
                    lemma <- getOrThrowError(t.lemma, "lemma was not set in: " + sourceFileName)
                    pos <- getOrThrowError(t.pos, "pos was not set in: " + sourceFileName)
                    ne <- getOrThrowError(t.ne, "ne was not set in: " + sourceFileName)
                } yield {
                    new Token(tokenIndex.next, startOffset, endOffset, surface, lemma, pos, pos.take(2), if (ne == "O") None else t.ne)
                }).merge
            }
            // Construct a sentence instance
            new Sentence(i, s.text, tokens)
        }

        // Read an annotation file
        val wordElems: NodeSeq = (XML.loadFile(annotationFile) \\ "word").filter(n => (n \\ "@wd").nonEmpty)
        val eventElems: NodeSeq = wordElems.filter(e => (e \\ "@eventid").nonEmpty)

        // Parse the annotation xml
        // Generate event objects
        val events: Seq[EventElement] = eventElems.map { e =>
            // eventid is start from 1
            val eventId: Int = (e \\ "@eventid").text.toInt
            val epistemicStatus: Option[EpistemicStatus.Value] = {
                // Handle the annotation error (missing epistemic status)
                if ((e \\ "@start").text == "584" && source.docId == "XIN_ENG_20030321.0417") {
                    Some(EpistemicStatus.AO)
                } else {
                    EpistemicStatus.uniformWithName((e \\ "@epistemic_status").text)
                }
            }
            new EventElement(
            eventId,
            (e \\ "@wd").text,
            EventType.uniformWithName((e \\ "@event_type").text),
            epistemicStatus,
            e \\ "@subevent_of" match {
                case i if i.nonEmpty => Some(i.text.toInt)
                case _ => None
            },
            e \\ "@in_reporting" match {
                case is if is.nonEmpty => is.text.split('+').map(_.toInt)
                case _ => Nil
            },
            e \\ "@coreference" match {
                case i if i.nonEmpty => Some(i.text.toInt)
                case _ => None
            },
            e \\ "@member_of" match {
                case is if is.nonEmpty => is.text.split('+').map(_.toInt)
                case _ => Nil
            },
            e \\ "@elliptical" match {
                case i if i.text == "yes" => true
                case _ => false
            })
        }

        // Connect an event to a token
        val words: Iterator[(String, Boolean, Int)] = wordElems.map { e =>
            val surface: String = (e \\ "@wd").text
            val isEvent = (e \\ "@eventid").nonEmpty
            if ((e \\ "@start").text == "") {
                println(annotationFile.getName)
                println(surface, (e \\ "@eventid").text)
            }
            val startOffset = (e \\ "@start").text.toInt
            (surface, isEvent, startOffset)
        }.sortBy(_._3).toIterator
        val eventWordsWithDummy = mutable.ArrayBuffer[(String, Option[EventElement])]()
        events.foreach { e =>
            val surface: String = e.surface
            var continue: Boolean = true
            while (continue && words.hasNext) {
                val word: (String, Boolean, Int) = words.next
                if (word._1 == surface) {
                    val eventOption: Option[EventElement] =
                        if (word._2) {
                            continue = false
                            Some(e)
                        } else None
                    eventWordsWithDummy += ((surface, eventOption))
                }
            }
        }
        val tokens: Iterator[Token] = sentences.flatMap(_.tokens).iterator
        eventWordsWithDummy.foreach { e =>
            val surface: String = e._1
            var continue: Boolean = true
            while (continue && tokens.hasNext) {
                val token: Token = tokens.next
                if (surface == token.surface) {
                    token.eventElement = e._2
                    continue = false
                }
            }
        }

        new Doc(source.docId, source.docType, source.date, sentences, rawText)
    }

}
