package util.tree

import doc.Event

import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
class Tree extends Node(null, null) {

    override val rootNode = this

    var nodes: ArrayBuffer[Node] = ArrayBuffer(this)

    override def add(e: Event): Node = {
        val node: Node = super.add(e)
        node.isClusterTop = true
        node
    }

}
