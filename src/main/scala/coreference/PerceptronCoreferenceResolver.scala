package coreference

import java.io.File

import config.EventCoreferenceConfig
import doc.{Doc, Event}
import ml.perceptron.{Parameter, PerceptronModel, Predictor, SolverType}
import reader.SourceFilesReader
import scopt.OptionParser
import takkbp.TbfFormatter
import util.FileOps

import scala.collection.immutable.Iterable
import scala.sys.process.Process
import scala.util.Either.RightProjection
import scala.util.Random

/**
  * Created by nakayama.
  */
object PerceptronCoreferenceResolver {

    def main(args: Array[String]): Unit = {
        val options: ArgOption = argParser(args)

        // Parse args
        if (options.config != null) {
            EventCoreferenceConfig.set(options.config)
        }

        val icDomainFiles: Array[File] = EventCoreferenceConfig.icDomainDir match {
            case Some(d) => FileOps.getFilesFromDir(d)
            case _ => throw new RuntimeException("IC domain dir setting was not found.")
        }
        val sourceFiles: Array[File] = EventCoreferenceConfig.sourceFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Source file extension of the IC domain setting was not found.")
        }
        val annotationFiles: Array[File] = EventCoreferenceConfig.annotationFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Annotation file extension of the IC domain setting was not found.")
        }

        // Prepare training and prediction data
        Random.setSeed(1)
        val shuffledSourceFiles: Array[File] = Random.shuffle(sourceFiles.toSeq).toArray
        val sourceFilesForTraining: Array[File] =
            shuffledSourceFiles.splitAt(EventCoreferenceConfig.numberOfTrainingFiles)._1.sorted
        val sourceFilesForPrediction: Array[File] =
            shuffledSourceFiles.splitAt(EventCoreferenceConfig.numberOfTrainingFiles)._2.sorted

        val annotationFilesForTraining: Array[File] = annotationFiles
        val annotationFilesForPrediction: Array[File] = annotationFiles

        val outputDir: File = FileOps.withMakeParentDirs(
            if (options.outputDir == null) {
                EventCoreferenceConfig.outputDir match {
                    case Some(d) if d != "" => new File(d)
                    case _ => throw new RuntimeException("Output directory setting was not found.")
                }
            } else options.outputDir)

        // Files for evaluation
        val tknDirPath: String = EventCoreferenceConfig.tknDirPath match {
            case Some(p) => p
            case _ => throw new RuntimeException("tkn dir path setting was not found.")
        }
        val goldTbfPath: String = EventCoreferenceConfig.goldTbfPath match {
            case Some(p) => p
            case _ => throw new RuntimeException("gold.tbf path setting was not found.")
        }

        if (options.paramTune) {
            //            parameterTuning(numberOfFolds = 5,
            //                sourceFilesForTraining, annotationFilesForTraining,
            //                tknDirPath, goldTbfPath,
            //                costs = Seq(0.01), // Seq(0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 10.0)
            //                biases = Seq(-1), // Seq(-1, 0, 1)
            //                outputDir)
        } else {
            // Set parameters
            def getOrThrowError[T](option: Option[T], message: String): RightProjection[Nothing, T] =
                option.toRight(throw new RuntimeException(message)).right
            val parameter: Parameter = (for {
                solverTypeStr <- getOrThrowError(EventCoreferenceConfig.coreferencePerceptronSolverType,
                    "perceptron solver type setting was not found.")
                numberOfIterations <- getOrThrowError(EventCoreferenceConfig.coreferencePerceptronNumberOfIterations,
                    "number of iterations of perceptron setting was not found.")
                beamWidth <- getOrThrowError(EventCoreferenceConfig.coreferencePerceptronBeamWidth,
                    "beam width of perceptron setting was not found.")
            } yield {
                val solverType: SolverType.Value = solverTypeStr match {
                    case "avg" => SolverType.AVERAGE
                    case s => throw new RuntimeException("unknown perceptron solver type: " + s)
                }
                new Parameter(solverType, numberOfIterations, beamWidth)
            }).merge

            mainProcess(sourceFilesForTraining, annotationFilesForTraining,
                sourceFilesForPrediction, annotationFilesForPrediction,
                parameter, outputDir)
            evaluate(tknDirPath, goldTbfPath, outputDir)
        }
    }

    //    final def parameterTuning(numberOfFolds: Int,
    //                              allSourceFiles: Array[File],
    //                              allAnnotationFiles: Array[File],
    //                              tknDirPath: String, goldTbfPath: String,
    //                              numberOfIterations: Seq[Int], beamWidth: Seq[Int],
    //                              outputDir: File) = {
    //        val nFoldedSourceFiles: Map[Symbol, Array[File]] =
    //            allSourceFiles.zipWithIndex.groupBy {
    //                case (tfs, i) => Symbol(s"n${i % numberOfFolds}")
    //            }.map {
    //                case (nFold, tknFilesWithIndex) => nFold -> tknFilesWithIndex.map(_._1)
    //            }
    //        for {
    //            numberOfIterations <- costs
    //            beamWidth <- beamWidthes
    //            (nFold, sourceFilesForPrediction) <- nFoldedSourceFiles
    //        } {
    //            val sourceFilesForTraining: Array[File] = nFoldedSourceFiles.collect {
    //                case (n, tfs) if n != nFold => tfs
    //            }.flatten.toArray
    //
    //            // Output directory
    //            val nFoldOutputDir: File = FileOps.withMakeParentDirs(
    //                s"${outputDir.getAbsolutePath}/c${cost}_b${beamWidth}_${nFold.name}")
    //
    //            mainProcess(sourceFilesForTraining, allAnnotationFiles,
    //                sourceFilesForPrediction, allAnnotationFiles,
    //                parameter,
    //                nFoldOutputDir)
    //            evaluate(tknDirPath, goldTbfPath, outputDir)
    //        }
    //    }

    final def mainProcess(sourceFilesForTraining: Array[File], annotationFilesForTraining: Array[File],
                          sourceFilesForPrediction: Array[File], annotationFilesForPrediction: Array[File],
                          parameter: Parameter,
                          outputDir: File): Unit = {
        // Parse files
        val allTrainDoc: Array[Doc] =
            new SourceFilesReader(sourceFilesForTraining).parseWithAnnotationFiles(annotationFilesForTraining)
        val allPredictDoc: Array[Doc] =
            new SourceFilesReader(sourceFilesForPrediction).parseWithAnnotationFiles(annotationFilesForPrediction)

        val perceptronModel: PerceptronModel = train(allTrainDoc, parameter)
        val predictedDocs: Map[Doc, Array[Array[Event]]] = predict(allPredictDoc, perceptronModel)

        // Output as tbf
        val tbfs: Iterable[String] =
            predictedDocs.map { case (d, eventCoref) => TbfFormatter.tbfFormatter(d, eventCoref) }
        FileOps.writeWithMakeParentDirs(new File(s"${outputDir.getAbsolutePath}/system.tbf")) {
            _.print(tbfs.mkString("\n"))
        }
    }

    final def train(docs: Array[Doc], parameter: Parameter): PerceptronModel =
        PerceptronModel.train(docs, parameter)

    final def predict(docs: Array[Doc], perceptronModel: PerceptronModel): Map[Doc, Array[Array[Event]]] = {
        val featureExtractor = CoreferenceFeatureExtractorForPerceptron(docs)

        val predictor: Predictor = new Predictor
        docs.map { d =>
            d -> predictor.predict(d, perceptronModel.weights, featureExtractor, perceptronModel.parameter.beamWidth)
                .eventCoreferences.map(_.events.toArray)
        }.toMap
    }

    final def evaluate(tknDirPath: String, goldTbfPath: String, outputDir: File) = {
        val outputDirPath: String = outputDir.getAbsolutePath
        val evaluateCommand: Seq[String] =
            EventCoreferenceConfig.evaluationScript match {
                case Some(s) =>
                    Seq("python", s,
                        "-g", goldTbfPath,
                        "-s", s"${outputDirPath}/system.tbf",
                        "-d", s"${outputDirPath}/comparison",
                        "-t", tknDirPath,
                        "-c", s"${outputDirPath}/conll-coref")
                case _ => throw new RuntimeException("evaluation script setting was not found.")
            }
        println("cmd: " + evaluateCommand)
        FileOps.write(new File(s"${outputDirPath}/result")) {
            val result = Process(evaluateCommand).lineStream_!.mkString("\n")
            println(result)
            _.println(result)
        }
    }

    final private[this] case class ArgOption(config: File = null,
                                             paramTune: Boolean = false,
//                                             params: Map[String, Double] = Map(),
                                             outputDir: File = null)

    final private[this] def argParser(args: Array[String]): ArgOption =
        new OptionParser[ArgOption](this.getClass.getName) {
            opt[File]('c', "config")
                .action((x, c) => c.copy(config = x))
                .validate { x =>
                    if (!x.exists) {
                        failure("configuration file was not found: " + x.getAbsolutePath)
                    } else success
                }
                .text("configuration file")

            opt[Unit]("param-tune")
                .action((_, c) => c.copy(paramTune = true))
                .text("parameter tune mode or not")

//            opt[Map[String, Double]]("params").optional
//                .valueName("cost=value,bias=value")
//                .action((x, c) => c.copy(params = x))
//                .validate(x =>
//                    x.keysIterator.find(k => k != "cost" && k != "bias") match {
//                        case Some(k) => failure("unknown parameter name: " + k)
//                        case _ => success
//                    })
//                .text("svm parameters")

            opt[File]('o', "output-dir").required()
                .valueName("<dir>")
                .action((x, c) => c.copy(outputDir = x))
                .validate { x =>
                    if (x.exists && x.isFile) {
                        failure(s"output-dir must be a directory. ${x.getAbsolutePath} is a file")
                    } else success
                }
                .text("output directory")
        }.parse(args, ArgOption()) match {
            case Some(x) => x
            case _ => System.exit(-1); null
        }

}
