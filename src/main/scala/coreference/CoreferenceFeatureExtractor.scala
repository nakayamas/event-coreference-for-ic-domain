package coreference

import doc.{Doc, Event}
import util.ml.liblinear.LiblinearInstance

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
final class CoreferenceFeatureExtractor {

    val samples: ArrayBuffer[LiblinearInstance] = ArrayBuffer[LiblinearInstance]()

    def createFeaturesForTrain(doc: Array[Doc]): Unit = doc.foreach(createFeaturesForTrain)

    def createFeaturesForTrain(doc: Doc): Unit = {
        val events: Seq[Event] = doc.events
        for {
            (i, n) <- events.zipWithIndex
            j <- events.take(n)
        } samples += new LiblinearInstance(if (i coreferTo j) "coref" else "non_coref", extractFeatures(i, j))
    }

    def extractFeatures(i: Event, j: Event): mutable.Map[String, Double] = {
        val features = mutable.Map.empty[String, Double]

        // Assume the events are within the same document
        if (i.docOfHeadToken != j.docOfHeadToken) {
            System.err.println(s"different docs: ${i} is in ${i.docOfHeadToken.id} but ${j} in in ${j.docOfHeadToken.id}")
        }
        val doc: Doc = i.docOfHeadToken

        features.put("same_surface", featureValueConverter(i.text == j.text))

        val eventDistance: Int = {
            val (startIndex, endIndex) = (i.headToken.index, j.headToken.index) match {
                case (n, m) if n > m => (m, n)
                case indices => indices
            }
            doc.tokens.slice(startIndex + 1, endIndex)
                .collect { case t if t.eventElement.isDefined => t.eventElement.get.event }.distinct.size
        }
        features.put("event_distance_" + eventDistance, 1)
        features.put("sentence_distance_" + (i.sentenceOfHeadToken.id - j.sentenceOfHeadToken.id).abs, 1)

        features.put("second_event_position_" + (j.sentenceOfHeadToken.id match {
            case p if p == 0 || p == 1 => p
            case _ => "no"
        }), 1)

        features.put("poses_" + Seq(i.headPos, j.headPos).sorted.mkString("_"), 1)

        features.put("same_pos", featureValueConverter(i.headPos == j.headPos))

        features.put("epistemic_status_" +
            Seq(i.epistemicStatus match {
                case Some(s) => s.toString
                //                case Some(EpistemicStatus.AO) => "AO"
                //                case Some(EpistemicStatus.AO) | Some(EpistemicStatus.NO) => "Occasion"
                //                case Some(EpistemicStatus.NO) | Some(EpistemicStatus.NEDO) | Some(EpistemicStatus.NOO) | Some(EpistemicStatus.NCO) => "Negation"
                //                case Some(EpistemicStatus.EDO) | Some(EpistemicStatus.NEDO) => "Expected"
                //                case Some(EpistemicStatus.OO) | Some(EpistemicStatus.NOO) => "Habitual"
                //                case Some(EpistemicStatus.CO) | Some(EpistemicStatus.NCO) => "Conditional"
                //                case Some(s) => "other"
                case _ => throw new RuntimeException("Missing epistemic status")
            }, j.epistemicStatus match {
                case Some(s) => s.toString
                //                case Some(EpistemicStatus.AO) => "AO"
                //                case Some(EpistemicStatus.AO) | Some(EpistemicStatus.NO) => "Occasion"
                //                case Some(EpistemicStatus.NO) | Some(EpistemicStatus.NEDO) | Some(EpistemicStatus.NOO) | Some(EpistemicStatus.NCO) => "Negation"
                //                case Some(EpistemicStatus.EDO) | Some(EpistemicStatus.NEDO) => "Expected"
                //                case Some(EpistemicStatus.OO) | Some(EpistemicStatus.NOO) => "Habitual"
                //                case Some(EpistemicStatus.CO) | Some(EpistemicStatus.NCO) => "Conditional"
                //                case Some(s) => "other"
                case _ => throw new RuntimeException("Missing epistemic status")
            }).sorted.mkString("_"), 1)

        features
    }

    private[this] def featureValueConverter(v: Any): Double =
        v match {
            case i: Int => i
            case d: Double => d
            case b: Boolean => if (b) 1 else 0
            case null => 0.0
        }

}
