package coreference

import java.io.File

import config.EventCoreferenceConfig
import de.bwaldvogel.liblinear._
import doc.{Doc, Event}
import reader.SourceFilesReader
import scopt.OptionParser
import takkbp.TbfFormatter
import util.FileOps
import util.ml.liblinear.{LiblinearFormatter, LiblinearMapper, Predictor}
import util.tree.Tree

import scala.collection.immutable.Iterable
import scala.sys.process.Process
import scala.util.Random

/**
  * Created by nakayama.
  */
object CoreferenceResolver {

    def main(args: Array[String]): Unit = {
        val options: ArgOption = argParser(args)

        // Parse args
        if (options.config != null) {
            EventCoreferenceConfig.set(options.config)
        }

        val icDomainFiles: Array[File] = EventCoreferenceConfig.icDomainDir match {
            case Some(d) => FileOps.getFilesFromDir(d)
            case _ => throw new RuntimeException("IC domain dir setting was not found.")
        }
        val sourceFiles: Array[File] = EventCoreferenceConfig.sourceFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Source file extension of the IC domain setting was not found.")
        }
        val annotationFiles: Array[File] = EventCoreferenceConfig.annotationFileExtension match {
            case Some(e) => icDomainFiles.filter(_.getName.endsWith(e))
            case _ => throw new RuntimeException("Annotation file extension of the IC domain setting was not found.")
        }

        // Prepare training and prediction data
        Random.setSeed(1)
        val shuffledSourceFiles: Array[File] = Random.shuffle(sourceFiles.toSeq).toArray
        val sourceFilesForTraining: Array[File] =
            shuffledSourceFiles.splitAt(EventCoreferenceConfig.numberOfTrainingFiles)._1.sorted
        val sourceFilesForPrediction: Array[File] =
            shuffledSourceFiles.splitAt(EventCoreferenceConfig.numberOfTrainingFiles)._2.sorted

        val annotationFilesForTraining: Array[File] = annotationFiles
        val annotationFilesForPrediction: Array[File] = annotationFiles

        val outputDir: File = FileOps.withMakeParentDirs(
            if (options.outputDir == null) {
                EventCoreferenceConfig.outputDir match {
                    case Some(d) if d != "" => new File(d)
                    case _ => throw new RuntimeException("Output directory setting was not found.")
                }
            } else options.outputDir)

        // Files for evaluation
        val tknDirPath: String = EventCoreferenceConfig.tknDirPath match {
            case Some(p) => p
            case _ => throw new RuntimeException("tkn dir path setting was not found.")
        }
        val goldTbfPath: String = EventCoreferenceConfig.goldTbfPath match {
            case Some(p) => p
            case _ => throw new RuntimeException("gold.tbf path setting was not found.")
        }

        if (options.paramTune) {
            parameterTuning(numberOfFolds = 5,
                sourceFilesForTraining, annotationFilesForTraining,
                tknDirPath, goldTbfPath,
                costs = Seq(0.01), // Seq(0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 10.0)
                biases = Seq(-1), // Seq(-1, 0, 1)
                outputDir)
        } else {
            val cost: Double =
                options.params.getOrElse("cost", EventCoreferenceConfig.coreferenceSvmTrainCost match {
                    case Some(x) => x
                    case _ => throw new RuntimeException("Training cost setting of Realis was not found.")
                })
            val bias: Double =
                options.params.getOrElse("bias", EventCoreferenceConfig.coreferenceSvmTrainBias match {
                    case Some(x) => x
                    case _ => throw new RuntimeException("Training bias setting of Realis was not found.")
                })

            mainProcess(sourceFilesForTraining, annotationFilesForTraining,
                sourceFilesForPrediction, annotationFilesForPrediction,
                cost, bias, outputDir)
            evaluate(tknDirPath, goldTbfPath, outputDir)
        }
    }

    final def parameterTuning(numberOfFolds: Int,
                              allSourceFiles: Array[File],
                              allAnnotationFiles: Array[File],
                              tknDirPath: String, goldTbfPath: String,
                              costs: Seq[Double], biases: Seq[Double],
                              outputDir: File) = {
        val nFoldedSourceFiles: Map[Symbol, Array[File]] =
            allSourceFiles.zipWithIndex.groupBy {
                case (tfs, i) => Symbol(s"n${i % numberOfFolds}")
            }.map {
                case (nFold, tknFilesWithIndex) => nFold -> tknFilesWithIndex.map(_._1)
            }
        for {
            cost <- costs
            bias <- biases
            (nFold, sourceFilesForPrediction) <- nFoldedSourceFiles
        } {
            val sourceFilesForTraining: Array[File] = nFoldedSourceFiles.collect {
                case (n, tfs) if n != nFold => tfs
            }.flatten.toArray

            // Output directory
            val nFoldOutputDir: File = FileOps.withMakeParentDirs(
                s"${outputDir.getAbsolutePath}/c${cost}_b${bias}_${nFold.name}")

            mainProcess(sourceFilesForTraining, allAnnotationFiles,
                sourceFilesForPrediction, allAnnotationFiles,
                cost, bias,
                nFoldOutputDir)
            evaluate(tknDirPath, goldTbfPath, outputDir)
        }
    }

    final def mainProcess(sourceFilesForTraining: Array[File], annotationFilesForTraining: Array[File],
                          sourceFilesForPrediction: Array[File], annotationFilesForPrediction: Array[File],
                          cost: Double, bias: Double,
                          outputDir: File): Unit = {
        // Parse files
        val allTrainDoc: Array[Doc] =
            new SourceFilesReader(sourceFilesForTraining).parseWithAnnotationFiles(annotationFilesForTraining)
        val allPredictDoc: Array[Doc] =
            new SourceFilesReader(sourceFilesForPrediction).parseWithAnnotationFiles(annotationFilesForPrediction)

        train(allTrainDoc, cost, bias, outputDir)
        val predictedDocs: Map[Doc, Array[Array[Event]]] = predict(allPredictDoc, outputDir)

        // Output as tbf
        val tbfs: Iterable[String] =
            predictedDocs.map { case (d, eventCoref) => TbfFormatter.tbfFormatter(d, eventCoref) }
        FileOps.write(new File(s"${outputDir.getAbsolutePath}/system.tbf")) {
            _.print(tbfs.mkString("\n"))
        }
    }

    final def train(docs: Array[Doc], cost: Double, bias: Double, outputDir: File): Unit = {
        // Prepare model and feature files
        val modelFile: File = new File(EventCoreferenceConfig.coreferenceSvmModelFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("SVM model file setting was not found.")
        })
        val liblinearFeatureFile: File = new File(EventCoreferenceConfig.coreferenceSvmLiblinearFeatureFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Liblinear feature file setting was not found.")
        })
        val classLabelIndexFile: File = new File(EventCoreferenceConfig.coreferenceSvmClassLabelIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Class label index file setting of the SVM model was not found.")
        })
        val featureNameIndexFile: File = new File(EventCoreferenceConfig.coreferenceSvmFeatureNameIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Feature name index file setting of the SVM model was not found.")
        })

        // Extract feature
        val featureExtractor: CoreferenceFeatureExtractor = new CoreferenceFeatureExtractor
        featureExtractor.createFeaturesForTrain(docs)

        // Map feature name to number (Liblinear format)
        val liblinearMapper: LiblinearMapper = LiblinearMapper(featureExtractor.samples)
        LiblinearFormatter(featureExtractor.samples, liblinearMapper).saveAsLiblinearTrainFormat(liblinearFeatureFile)
        liblinearMapper.saveClassLabelIndex(classLabelIndexFile)
        liblinearMapper.saveFeatureNameIndex(featureNameIndexFile)

        // Train with LibLinear
        val parameter = new Parameter(SolverType.L2R_LR_DUAL, cost, Double.PositiveInfinity)
        val problem = Problem.readFromFile(liblinearFeatureFile, bias)
        val model = Linear.train(problem, parameter)
        model.save(modelFile)
    }

    final def predict(docs: Array[Doc], modelAndFeatureDir: File): Map[Doc, Array[Array[Event]]] = {
        // Read model and feature files
        val modelFile: File = new File(EventCoreferenceConfig.coreferenceSvmModelFile match {
            case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("SVM model file setting was not found.")
        })
        val classLabelIndexFile: File = new File(EventCoreferenceConfig.coreferenceSvmClassLabelIndexFile match {
            case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Class label index file setting of the SVM model was not found.")
        })
        val featureNameIndexFile: File = new File(EventCoreferenceConfig.coreferenceSvmFeatureNameIndexFile match {
            case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Feature name index file setting of the SVM model was not found.")
        })

        // Load model and features
        val model: Model = Model.load(modelFile)
        val liblinearMapper = LiblinearMapper(classLabelIndexFile, featureNameIndexFile)
        val predictor = new Predictor(model, liblinearMapper)
        val extractor = new CoreferenceFeatureExtractor
        (for {
            doc <- docs
        } yield {
            // predict and create a tree
            val tree = new Tree
            val events = doc.events
            events.indices.foreach { n =>
                val i: Event = events(n)
                var mostProbableEvent: (Event, Double) = (null, 0.0)
                for {
                    j: Event <- events.take(n)
                } {
                    val (label, probability) = predictor.predictProbability(extractor.extractFeatures(i, j).toMap)
                    if (label == "coref" && mostProbableEvent._2 < probability) {
                        mostProbableEvent = (j, probability)
                    }
                }
                if (mostProbableEvent._1 == null) {
                    // create a new cluster
                    tree.add(i)
                } else {
                    // append to an exist node
                    tree.nodes.find(_.event == mostProbableEvent._1).get.add(i)
                }
            }

            // Create event coreference clusters
            doc -> tree.nodes.filter(_.isClusterTop).map(_.subNodes.map(_.event)).toArray
        }).toMap
    }

    final def evaluate(tknDirPath: String, goldTbfPath: String, outputDir: File) = {
        val outputDirPath: String = outputDir.getAbsolutePath
        val evaluateCommand: Seq[String] =
            EventCoreferenceConfig.evaluationScript match {
                case Some(s) =>
                    Seq("python", s,
                        "-g", goldTbfPath,
                        "-s", s"${outputDirPath}/system.tbf",
                        "-d", s"${outputDirPath}/comparison",
                        "-t", tknDirPath,
                        "-c", s"${outputDirPath}/conll-coref")
                case _ => throw new RuntimeException("evaluation script setting was not found.")
            }
        println("cmd: " + evaluateCommand)
        FileOps.write(new File(s"${outputDirPath}/result")) {
            val result = Process(evaluateCommand).lineStream_!.mkString("\n")
            println(result)
            _.println(result)
        }
    }

    final private[this] case class ArgOption(config: File = null,
                                             paramTune: Boolean = false,
                                             params: Map[String, Double] = Map(),
                                             outputDir: File = null)

    final private[this] def argParser(args: Array[String]): ArgOption =
        new OptionParser[ArgOption](this.getClass.getName) {
            opt[File]('c', "config")
                .action((x, c) => c.copy(config = x))
                .validate { x =>
                    if (!x.exists) {
                        failure("configuration file was not found: " + x.getAbsolutePath)
                    } else success
                }
                .text("configuration file")

            opt[Unit]("param-tune")
                .action((_, c) => c.copy(paramTune = true))
                .text("parameter tune mode or not")

            opt[Map[String, Double]]("params").optional
                .valueName("cost=value,bias=value")
                .action((x, c) => c.copy(params = x))
                .validate(x =>
                    x.keysIterator.find(k => k != "cost" && k != "bias") match {
                        case Some(k) => failure("unknown parameter name: " + k)
                        case _ => success
                    })
                .text("svm parameters")

            opt[File]('o', "output-dir").required()
                .valueName("<dir>")
                .action((x, c) => c.copy(outputDir = x))
                .validate { x =>
                    if (x.exists && x.isFile) {
                        failure(s"output-dir must be a directory. ${x.getAbsolutePath} is a file")
                    } else success
                }
                .text("output directory")
        }.parse(args, ArgOption()) match {
            case Some(x) => x
            case _ => System.exit(-1); null
        }

}
