package coreference

import doc.{CoreferenceTree, Doc, Event, EventCoreference}

import scala.collection.mutable

/**
  * Created by nakayama.
  */
final case class CoreferenceFeatureExtractorForPerceptron(docs: Array[Doc]) {

    type Feature = Map[String, Double]

    val eventToFeatures = mutable.Map.empty[Event, Feature]

    val docToCoreferenceTree: Map[Doc, CoreferenceTree] = docs.map(d =>
        d -> new CoreferenceTree(d, d.eventCoreferences.toArray, extractFeatures(d.eventCoreferences))
    ).toMap

    def extractFeatures(eventCoreferences: Array[EventCoreference]): Feature =
        extractFeatures(eventCoreferences.map(_.events.toArray))

    def extractFeatures(eventCoreferences: Array[Array[Event]]): Feature = {
        val features = mutable.Map.empty[String, Double]
        features.put("tree_size", eventCoreferences.length)
        eventCoreferences.foreach { c =>
            // In a coreference cluster
//            features.put("same_surface_in_the_same_cluster", c.surface)
            c.foreach {
                // In an event
                extractFeatures(_).toSeq.foreach { case (k, v) => features.put(k, v) }
            }
        }
        features.toMap
    }

    def extractFeatures(event: Event): Feature = eventToFeatures.get(event) match {
        case Some(f) => f
        case _ => {
            val features = mutable.Map.empty[String, Double]
            features.put("surface_" + event.text.replaceAllLiterally(" ", "_"), 1.0)
            features.toMap
        }
    }

    def extractFeatures(i: Event, j: Event): mutable.Map[String, Double] = {
        val features = mutable.Map.empty[String, Double]

        // Assume the events are within the same document
        if (i.docOfHeadToken != j.docOfHeadToken) {
            System.err.println(s"different docs: ${i} is in ${i.docOfHeadToken.id} but ${j} in in ${j.docOfHeadToken.id}")
        }
        val doc: Doc = i.docOfHeadToken

        features.put("same_surface", featureValueConverter(i.text == j.text))

        val eventDistance: Int = {
            val (startIndex, endIndex) = (i.headToken.index, j.headToken.index) match {
                case (n, m) if n > m => (m, n)
                case indices => indices
            }
            doc.tokens.slice(startIndex + 1, endIndex)
                .collect { case t if t.eventElement.isDefined => t.eventElement.get.event }.distinct.size
        }
        features.put("event_distance_" + eventDistance, 1)
        features.put("sentence_distance_" + (i.sentenceOfHeadToken.id - j.sentenceOfHeadToken.id).abs, 1)

        features.put("second_event_position_" + (j.sentenceOfHeadToken.id match {
            case p if p == 0 || p == 1 => p
            case _ => "no"
        }), 1)

        features.put("poses_" + Seq(i.headPos, j.headPos).sorted.mkString("_"), 1)

        features.put("same_pos", featureValueConverter(i.headPos == j.headPos))

        features.put("epistemic_status_" +
            Seq(i.epistemicStatus match {
                case Some(s) => s.toString
                //                case Some(EpistemicStatus.AO) => "AO"
                //                case Some(EpistemicStatus.AO) | Some(EpistemicStatus.NO) => "Occasion"
                //                case Some(EpistemicStatus.NO) | Some(EpistemicStatus.NEDO) | Some(EpistemicStatus.NOO) | Some(EpistemicStatus.NCO) => "Negation"
                //                case Some(EpistemicStatus.EDO) | Some(EpistemicStatus.NEDO) => "Expected"
                //                case Some(EpistemicStatus.OO) | Some(EpistemicStatus.NOO) => "Habitual"
                //                case Some(EpistemicStatus.CO) | Some(EpistemicStatus.NCO) => "Conditional"
                //                case Some(s) => "other"
                case _ => throw new RuntimeException("Missing epistemic status")
            }, j.epistemicStatus match {
                case Some(s) => s.toString
                //                case Some(EpistemicStatus.AO) => "AO"
                //                case Some(EpistemicStatus.AO) | Some(EpistemicStatus.NO) => "Occasion"
                //                case Some(EpistemicStatus.NO) | Some(EpistemicStatus.NEDO) | Some(EpistemicStatus.NOO) | Some(EpistemicStatus.NCO) => "Negation"
                //                case Some(EpistemicStatus.EDO) | Some(EpistemicStatus.NEDO) => "Expected"
                //                case Some(EpistemicStatus.OO) | Some(EpistemicStatus.NOO) => "Habitual"
                //                case Some(EpistemicStatus.CO) | Some(EpistemicStatus.NCO) => "Conditional"
                //                case Some(s) => "other"
                case _ => throw new RuntimeException("Missing epistemic status")
            }).sorted.mkString("_"), 1)

        features
    }

    private[this] def featureValueConverter(v: Any): Double = v match {
        case i: Int => i
        case d: Double => d
        case b: Boolean => if (b) 1 else 0
        case null => 0.0
    }

}
