package reader

import java.io.File

import doc._
import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source

/**
  * Created by nakayama.
  */
class SourceFileReaderTest extends FlatSpec with Matchers {

    val sourceFilePath = "src/test/resources/test.src.xml.txt"
    val sourceFile: File = new File(sourceFilePath)
    val annotationFile: File = new File("src/test/resources/test.src.xml.txt.blk.tok.stp.tbf.xml")

    val doc: Doc = SourceFileReader.parseWithAnnotationFile(sourceFile, annotationFile)

    "Parsed data" should "have expected data" in {
        doc.id shouldBe "TEST.DOCUMENT"
        doc.date shouldBe "2016-01-25"
        doc.rawText shouldBe Source.fromFile(sourceFile).getLines.mkString("\n")
    }

    "Sentences of the parsed data" should "have expected data" in {
        doc.sentences.length shouldBe 5

        val sentence1: Sentence = doc.sentences(1)
        sentence1.id shouldBe 1
        sentence1.text shouldBe """This is a (test) document."""
        sentence1.tokens.length shouldBe 8
        val sentence3: Sentence = doc.sentences(3)
        sentence3.text shouldBe """"Third sentence" is here."""
    }

    "Tokens of the parsed data in the second sentence" should "have expected data" in {
        val tokens: Seq[Token] = doc.sentences(1).tokens

        val token0: Token = tokens.head
        token0.index shouldBe 5
        token0.startOffset shouldBe 20
        token0.endOffset shouldBe 24
        token0.surface shouldBe "This"
        token0.pos shouldBe "DT"
        token0.pos2 shouldBe "DT"
        token0.lemma shouldBe "this"
        token0.ne shouldBe None

        val token3: Token = tokens(3)
        token3.surface shouldBe "("
        token3.lemma shouldBe "-lrb-"

        val token5: Token = tokens(5)
        token5.surface shouldBe ")"
        token5.lemma shouldBe "-rrb-"
    }

    "Tokens of the parsed data in the 4th sentence" should "have expected data" in {
        val tokens: Seq[Token] = doc.sentences(3).tokens

        val token0: Token = tokens.head
        token0.surface shouldBe "\""
        token0.lemma shouldBe "``"

        val token3: Token = tokens(3)
        token3.surface shouldBe "\""
        token3.lemma shouldBe "''"
    }

    "Event words" should "have expected data" in {
        val eventTokens: Seq[Token] = doc.eventTokens

        val eventToken0: Token = eventTokens.head
        eventToken0.surface shouldBe "is"
        eventToken0.eventElement shouldBe defined
        val event0: EventElement = eventToken0.eventElement.get
        event0.eventId shouldBe 1
        event0.epistemicStatus shouldBe defined
        event0.epistemicStatus.get.toString shouldBe "AO"
        event0.eventType shouldBe defined
        event0.eventType.get.toString shouldBe "EVENT"
        event0.parentEventId shouldBe None
        event0.inReportingId shouldBe Nil
        event0.coreferenceId shouldBe None
        event0.memberIds shouldBe Nil
        event0.isElliptical shouldBe false
        event0.eventTokens shouldBe Seq(eventToken0)

        val eventToken2: Token = eventTokens(2)
        eventToken2.surface shouldBe "Second"
        eventToken2.eventElement shouldBe defined
        val event2: EventElement = eventToken2.eventElement.get
        event2.epistemicStatus shouldBe defined
        event2.epistemicStatus.get.toString shouldBe "OO"

        val eventToken3: Token = eventTokens(3)
        eventToken3.surface shouldBe "sentence"
        eventToken3.eventElement shouldBe defined
        val event3: EventElement = eventToken3.eventElement.get
        event3.epistemicStatus shouldBe defined
        event3.epistemicStatus.get.toString shouldBe "OO"

        event2.events shouldBe event3.events
        event2.events shouldBe Seq(event2, event3)
        event2.eventTokens shouldBe Seq(eventToken2, eventToken3)
        event3.eventTokens shouldBe Seq(eventToken2, eventToken3)

        val eventToken6: Token = eventTokens(6)
        eventToken6.index shouldBe 26
        eventToken6.prevToken shouldBe defined
        eventToken6.prevToken.get.surface shouldBe "and"
        eventToken6.nextToken shouldBe defined
        eventToken6.nextToken.get.surface shouldBe "."
        eventToken6.eventElement shouldBe defined
        val event6: EventElement = eventToken6.eventElement.get
        event6.eventType shouldBe defined
        event6.eventType.get.toString shouldBe "IMPLICIT"
        event6.epistemicStatus shouldBe defined
        event6.epistemicStatus.get.toString shouldBe "AO"
    }

    "Events" should "have expected data" in {
        val events: Seq[Event] = doc.events

        val event0: Event = events.head
        event0.text shouldBe "is"
        event0.eventId shouldBe 1
        event0.epistemicStatus shouldBe defined
        event0.epistemicStatus.get.toString shouldBe "AO"
        event0.eventType shouldBe defined
        event0.eventType.get.toString shouldBe "EVENT"
        event0.parentEventId shouldBe None
        event0.inReportingId shouldBe Nil
        event0.coreferenceId shouldBe None
        event0.memberIds shouldBe Nil
        event0.isElliptical shouldBe false

        val event2: Event = events(2)
        event2.text shouldBe "Second sentence"
        event2.epistemicStatus shouldBe defined
        event2.epistemicStatus.get.toString shouldBe "OO"
        event2.eventType shouldBe defined
        event2.eventType.get.toString shouldBe "EVENT"
        event2.parentEventId shouldBe None
        event2.inReportingId shouldBe Nil
        event2.coreferenceId shouldBe None
        event2.memberIds shouldBe Nil
        event2.isElliptical shouldBe false

        event0 coreferTo event2 shouldBe false
        event0 coreferTo events.head shouldBe true
    }

    "EventCoreference" should "have expected data" in {
        val events: Seq[Event] = doc.events
        val eventCoreferences: Seq[EventCoreference] = doc.eventCoreferences

        val eventCoreference0: EventCoreference = eventCoreferences.head
        eventCoreference0.events should contain theSameElementsInOrderAs Seq(events.head, events(1))

        val eventCoreference1: EventCoreference = eventCoreferences(1)
        eventCoreference1.events should contain theSameElementsInOrderAs Seq(events(2), events(3), events(4))
    }

}
